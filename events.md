---
tags: page
layout: base.njk
title: Events
url: /events/
---

## Event

![ Events](/img/event_image_2a.jpg)






<button class="accordion"> Upcoming </button>
    <div class="panel">
      Student Pharam presents
         Pharma Hackathon 2018
         Option 1: Full Day Tutorial on Bio/ Cheminformatics and in-silico Drug Design
         Option 2: Take the Challenge
         Date: November 17, 2018
         Time: 8:30am - 8:00pm
         Location: New Residence Hall: Ballroom, 3625 Park Ave, Montreal, Quebec, Canada, H2X 3P8
         <a href="#">Read More</a>
       </div>


          MiCM symposium Nov 19th, 2018.
          Research Institute of the MUHC - Glen site
          1001 Decarie boul
          Cruess Amphitheatre
          Block E, Room E S1.1129
          Montreal, QC
          H4A 3J1
          <a href="#"> Read more</a>










        </div>


<div class="bottomspace"> </div>
