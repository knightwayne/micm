---
tags: page
layout: base.njk
title: Contact
url: /contact/
---

![MiCM Map Location](/img/micm_location.jpg)



**McGill initiative in Computational Medicine** <br>
740 Dr. Penfield Avenue <br>
Montreal, Quebec <br>
Canada, H3A 0G1 <br>
email: [info-micm@mcgill.ca](mailto:info-micm@mcgill.ca)
