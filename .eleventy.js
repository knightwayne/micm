/*
 * .eleventy.js
 */

const pageOrdering = ["Home", "About", "Profiles", "Events", "Job Opportunities", "Classes", "UNIWeb", "Contact"];

module.exports = function (eleventyConfig) {
    eleventyConfig.addCollection("page", collection => {
        return collection.getFilteredByTag("page").sort((a, b) => {
            return pageOrdering.indexOf(a.data.title) - pageOrdering.indexOf(b.data.title);
        });
    });

    return {
        templateFormats: [
            'md',
            'css', // css is not yet a valid template extension
            'png',
            'jpeg',
            'jpg',
            'gif',
            'svg',
            'json',
            'js',
        ],
        passthroughFileCopy: true
    };
};
