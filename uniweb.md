---
tags: page
layout: base.njk
title: UNIWeb
url: /uniweb/
---

### Welcome to UNIWeb 

Are you a McGill Researcher who expressed an interest in joining MiCM? This is how you can participate. On behalf of
Drs. Guillaume Bourque and David Buckeridge, co-lead of the MiCM, we invite Researchers who have expressed an interest
in joining MiCM to do so by adding their information on UNIWeb [(https://uniweb.mcgill.ca/)](https://uniweb.mcgill.ca/).

The information collected on UNIWeb will facilitate the compilation of Research Interests from different researchers and
display them in a unique way on the MiCM website. The goal of this exercise is to enhance collaborations by sharing and
transferring of knowledge within the Faculty of Medicine and the broader McGill campus.

Please refer to the <strong>PDF</strong> document for the procedure to setup your UNIWeb account, available
<a href="https://www.mcgill.ca/micm/files/micm/uniweb_instruction_ready_v1.pdf">[here]</a>.

<a href="https://www.mcgill.ca/micm/files/micm/uniweb_instruction_ready_v1.pdf"><img src="/img/cover_image_v2.jpg"></a>
