let allClasses;
let searchClasses;
let searchTerm = "";

const fields = ["code", "title", "offered_by", "target_audience", "background", "format", "credits", "format_type",
    "length", "general_description", "topics", "subtopics", "software", "prerequisites", "contact_info", "name",
    "email", "open_to", "feedback", "contact_instructors", "notes", "contacted_professor"];

const shortValueFields = ["code", "title", "offered_by", "credits", "format_type", "topics", "subtopics", "software",
    "open_to"];

const updateClasses = () => {
    d3.select("#classes-count").text(`(${searchClasses.length.toString()})`);

    const classes = d3.select("#classes-list")
        .selectAll(".class-item")
        .data(searchClasses, c => c["code"]);

    const newClass = classes.enter()
        .append("div")
        .classed("class-item", true);

    const classHeader = newClass.append("h2");
    classHeader.append("span")
        .classed("class-code", true)
        .classed("hidden", c => !c["code"])
        .text(c => c["code"]);

    classHeader.append("span")
        .classed("class-title", true)
        .text(c => c["title"]);

    classHeader.append("span")
        .classed("class-credits", true)
        .classed("hidden", c => !c["credits"])
        .text(c => `(${c["credits"]} credits)`);

    newClass.append("p").attr("style", "margin: 5px 0;")
        .html(c => `<strong>Offered By:</strong> ${c["offered_by"]}`);

    newClass.append("p").attr("style", "margin: 5px 0;")
        .html(c => `<strong>Target Audience:</strong> ${c["target_audience"]}`);

    newClass.append("p").attr("style", "margin: 5px 0;")
        .html(c => `<strong>Topic(s):</strong> ${c["topics"]}`);

    newClass.append("p").attr("style", "margin: 5px 0;")
        .html(c => `<strong>Prerequisites:</strong> ${c["prerequisites"]}`);

    newClass.append("p").attr("style", "margin-top: 5px;")
        .html(c => `<strong>Assumed Background:</strong> ${c["background"]}`);

    newClass.append("p").html(c => c["general_description"]);

    newClass.append("p").html(c => `<strong>Open To:</strong> ${c["open_to"]}`);

    classes.exit().remove();
};

const refreshSearch = () => {
    const codedSearch = searchTerm.split("=").length === 2
        && fields.includes(searchTerm.split("=")[0].toLocaleLowerCase());

    console.log(codedSearch, searchTerm.split("="));

    if (!codedSearch) {
        // TODO

        updateClasses();
        return;
    }

    const field = searchTerm.split("=")[0].toLocaleLowerCase();
    const term = searchTerm.split("=")[1].toLocaleLowerCase();

    searchClasses = [];

    allClasses.forEach(c => {
        if (!c[field]) return;
        let classTerm = c[field].toString().toLocaleLowerCase();
        if (!classTerm.includes(term)) return;
        searchClasses.push(c);
    });

    // TODO
    updateClasses();
};


const buildFilter = data => {
    if (document.getElementById("classes-list") === null) return;

    allClasses = data;
    searchClasses = data;

    updateClasses();


    // Events

    d3.select("#filter-bar").on("focus", () => {
        d3.select("#classes-suggestions").classed("show", true);
    });

    // d3.select("#classes-filter").on("mouseout", () => {
    //     d3.select("#classes-suggestions").classed("show", false);
    // });

    d3.select("#filter-bar").on("focusout", () => {
        // d3.select("#classes-suggestions").classed("show", false);
    });

    d3.select("#filter-bar").on("keypress", () => {
        searchTerm = d3.event.target.value.toLocaleLowerCase();
        if (searchTerm.length <= 3) {
            d3.select("#classes-suggestions").select("ul").html("");
            d3.select("#classes-suggestions").select("span").attr("style", "display: inline;");
            return;
        }

        d3.select("#classes-suggestions").select("span").attr("style", "display: none;");

        // searchClasses = [];
        console.log(searchTerm);

        const possibleFilters = new Set();

        allClasses.forEach(c => {
            let found = false;
            shortValueFields.forEach(f => {
                if (!c[f]) return;
                let classTerm = c[f].toString().toLocaleLowerCase();
                if (!classTerm.includes(searchTerm)) return;

                found = true;

                possibleFilters.add(`${f}=${classTerm}`);
            });
        });

        let filters = d3.select("#classes-suggestions").select("ul").selectAll("li")
            .data([...possibleFilters], f => f);

        filters.enter().append("li").text(f => f).on("click", f => {
            d3.select("#filter-bar").property("value", f);
            searchTerm = f;
            d3.select("#classes-suggestions").classed("show", false);
            refreshSearch();
        });
        filters.exit().remove();

        console.log(possibleFilters);
    });

    d3.select("#filter-bar").on("change", () => {
        searchTerm = d3.event.target.value.toLocaleLowerCase();
        searchClasses = [];

        allClasses.forEach(c => {
            let found = false;
            fields.forEach(f => {
                if (!c[f]) return;
                let classTerm = c[f].toString().toLocaleLowerCase();
                if (!classTerm.includes(searchTerm)) return;

                found = true;
            });

            if (found) {
                searchClasses.push(c);
            }
        });

        refreshSearch();
    });
};

document.addEventListener("DOMContentLoaded", () => {
    d3.json("/data/courses.json").then(buildFilter);
});


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
