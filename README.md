# MiCM Website

## Dependencies

Compiling the website requires [Eleventy](https://www.11ty.io/), a static site
generator written in JavaScript. Changes in Markdown can be committed without
compiling, but the changes will not be visible in the generated HTML.

To install Eleventy, first make sure NodeJS and NPM are installed on your
development machine. Then, install Eleventy with the following command:

```bash
npm install -g eleventy
```

This command may need to be run with `sudo` privileges.


## Project Structure

TODO


## Editing the Website

TODO
