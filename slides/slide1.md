---
tags: slide
layout: slide.njk
title: McGill initiative in Computational Medicine (MiCM)
image: slider_1.jpg
alt: MiCM Slide
---

To respond to the opportunity of a better use of data and computational methods
in Medicine, we have launched a new structuring project within the University:
the McGill initiative in Computational Medicine.
