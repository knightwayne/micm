---
tags: page
layout: base.njk
title: Job Opportunities
url: /job_opportunities/
---


![Job Opportunities page header](/img/job_opportunities.png)


#### Neurostatistician

We are seeking a creative and broad-thinking data scientist to collaborate with a dynamic team of early career
investigators within the Ludmer Centre for Neuroinformatics & Mental Health (www.LudmerCentre.ca), at McGill University.
The Centre promotes the application of interdisciplinary, neuroinformatics approaches to research into brain development
and neurological disorders and mental illness. You will be embedded within a global collaborative network in an open,
dynamic, collegial and team-oriented environment. You will contribute to analyses of multi-omic datasets, development of
new data integration tools and help advance pan-Ludmer research collaborations...

[learn more](https://www.mcgill.ca/micm/files/micm/neurostatistician_posting.pdf)


---


#### Assistant or Associate Professor

The Department of Physiology at McGill University is seeking applicants for a tenure-track faculty position at the
Assistant/Associate Professor rank. The Department of Physiology is seeking applicants who apply artificial
intelligence/machine learning to physiological systems. The projected start date for this position is August 1, 2019.

<a href="https://www.mcgill.ca/medicine-academic/files/medicine-academic/20181218_advertisement_rl_18082p-181102_eng_final.pdf">Learn more ></a>


---


#### Research Associate/Postdoctoral

Applications are invited for a Research Associate/Postdoctoral Fellow in the Brown lab, in collaboration with the
Advanced BioImaging Facility at McGill University in Montreal, Canada. Candidates should have a Ph.D. degree in the
areas of Biophysics, Cell Biology, Cancer Biology, Bioengineering, Computer Science or a related field with an excellent
track record for innovative research.

<a href="https://www.mcgill.ca/micm/files/micm/post_doc_research_associate.pdf">Learn more ></a>


---


###  Computational postdoctoral fellow in Comparative Epigenomics

The Bourque lab is seeking a computational postdoctoral fellow interested in analyzing such primate comparative
epigenomics datasets. A component of the project will be to develop new analytical methods as needed. PhD training
should be in bioinformatics, computational genomics or equivalent and required skills include programming (Python or
equivalent), statistics and R. Prior experience in genomics and/or epigenomics data analysis is highly desirable.
Position will be funded for 2-years with possible extensions.

<a href="http://www.computationalgenomics.ca/BourqueLab/computational-postdoctoral/"> Learn more</a>


---


### Computational postdoctoral fellow in Cancer Genomics

The Bourque lab is seeking a computational postdoctoral fellow interested in analyzing cancer genomics and epigenomics
datasets. A component of the project will be to develop new analytical methods as needed. PhD training should be in
bioinformatics, computational genomics or equivalent and required skills include programming (Python or equivalent),
statistics and R. Prior experience in genomics and/or epigenomics data analysis is highly desirable. Position will be
funded for 2-years with possible extensions.

<a href="http://www.computationalgenomics.ca/BourqueLab/computational-postdoctoral-fellow-in-cancer/"> Learn more</a>


---


### Master Fellowship or PhD position

Dr Sapir Pichhadze in the Research Institute of the McGill University Health Centre is currently seeking a talented
candidate to develop applications in era of precision medicine. The lab research program is translational in nature and
focuses on the application of precision medicine tools for the prevention of immune-mediated injuries in transplant
recipients...

<a href="https://www.mcgill.ca/micm/files/micm/master_fellow_or_phd_position.pdf"> Download to learn more.</a>


---


### Statistical Research Assistant

A research assistant is sought to work with Dr. Sapir-Pichhadze on the Genome Canada funded Large Scale Applied Research
Project,Precision Medicine Can PREVENT AMR, which represents a national initiative for the development and
implementation of tools, approaches and techniques aimed at the prevention of kidney transplant rejection...

<a href="https://www.mcgill.ca/micm/files/micm/statistical_research_assistant.pdf">Learn more</a>
