#!/usr/bin/env python3

import csv
import json
import re
import sys


RE_COURSE_NAME = re.compile(r"^([A-Z]+[\s]*[0-9/D]+)?[:]?\s*(.+)$")
RE_CREDITS = re.compile(r"^([0-9]+)\s*(.+)$")


def usage():
    print("Usage: {} file_to_process.csv file_to_output.json".format(sys.argv[0]))
    exit(1)


def process_text(unp: str) -> str:
    return unp.replace("\n", "").replace("\u00a0", " ").replace("\u2018", "'").replace("\u2019", "'") \
        .replace("\t", "").strip()


def main(args):
    file_path = args[0]
    file_out = args[1]
    with open(file_path, "r", newline="") as fi, open(file_out, "w") as fo:
        course_data = []

        next(fi)
        reader = csv.DictReader(fi)

        for row in reader:
            name = row["Course Name"].strip().replace("\xa0", " ")

            try:
                code, title = re.match(RE_COURSE_NAME, name).group(1, 2)
            except AttributeError:
                continue

            try:
                f_credits, format_type = re.match(RE_CREDITS, row["Format (coded)"]).group(1, 2)
                format_type = format_type.strip().lower()
            except AttributeError:
                f_credits, format_type = None, None

            course = {
                "code": code,
                "title": title,
                "offered_by": process_text(row["Department or group offering"]),
                "target_audience": process_text(row["Target Audience"]),
                "background": row["Assumed Technical Background relevant to the course (Limited, Moderate, "
                                  "Expert)"].strip(),
                "format": process_text(row["Format"]),
                "credits": f_credits,
                "format_type": format_type,
                "length": row["Length (coded)"].strip(),
                "general_description": process_text(row["General description of topics covered"]),
                "topics": row["Topics (coded)"].strip().lower(),
                "subtopics": row["Subtopics (coded)"].strip().lower(),
                "software": process_text(row["Software (coded)"]).lower(),
                "prerequisites": process_text(row["Prerequisites"]),
                "contact_info": {
                    "name": row["Contact name"].strip(),
                    "email": (row["Contact email"].lower().replace(" ", "").replace(",", ";").split(";")
                              if row["Contact email"] not in ("NA", "") else None)
                },
                "open_to": process_text(row["Who is the course open to"]),
                "feedback": process_text(row["Feedback from students?"]),
                "contact_instructors": row["contact instructors to ask more details about who it is "
                                           "for/outline/student background"].strip(),
                "notes": row["Notes"].strip(),
                "contacted_professor": (row["contacted Professor (Y/N)"].strip() == "Y"
                                        if row["contacted Professor (Y/N)"].strip() != "" else None)
            }

            if course["offered_by"] == "" and course["target_audience"] == "":
                # Heuristic for "bad" row
                continue

            course_data.append(course)

        json.dump(course_data, fo)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
    main(sys.argv[1:])
