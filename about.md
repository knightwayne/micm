---
tags: page
layout: base.njk
title: About
url: /about/
---

# About

## McGill initiative in Computational Medicine

Data collection in the life sciences, including biomedical research in the
laboratory and healthcare in the clinic, is taking place at an unprecedented
scale.  To respond to the opportunity of a better use of data and computational
methods in Medicine, we created a new Initiative within the Faculty of
Medicine:  the McGill initiative in Computational Medicine (MiCM).

## Our mission

The main objective of MiCM will be to create novel interdisciplinary research
interfaces and carry out innovative research programs in all aspects of
Computational Medicine and the use of data in health research and healthcare
delivery. 
 
The MiCM will integrate patient based datasets acquired in the clinical setting
with fundamental research results in fields such as genomics, imaging and other
areas of high throughput biology, and this using novel computational tools,
methods and unique interdisciplinary grouping. It will also coordinate the
deployment of large computational resources that are needed for high-throughput
biology and modern Medicine.

![MiCM Steering Committee](/img/micm_steering.jpg)
