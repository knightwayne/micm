---
tags: page 
layout: base.njk
title: Classes
url: /classes/
---

<h1>Classes <span id="classes-count">(0)</span></h1>

<div class="box" id="classes-filter">
    <h2>Filter Classes</h2>
    <div id="classes-search">
        <label for="filter-bar">Search Classes:</label>
        <input type="text" name="filter-bar" id="filter-bar">
    </div>
    <div id="classes-suggestions">
        <h3>Suggested Filters</h3>
        <span>Keep typing...</span>
        <ul></ul>
    </div>
</div>
<div id="classes-list"></div>
