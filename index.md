---
layout: home.njk
tags: page
title: Home
url: /
---

In case you missed it this year's Symposium, you can watch it on YouTube 
[Watch NOW](https://www.youtube.com/watch?v=l2J5-gysupg).

---

Are you a McGill Researcher who expressed an interest in joining MiCM? This is
how you can participate. On behalf of Drs. Guillaume Bourque and David
Buckeridge, co-leads of the MiCM, we invite researchers who have expressed an
interest in joining MiCM to do so by adding their information on UNIWeb.
[Learn more...](https://www.mcgill.ca/micm/uniweb-instruction-guide)

Remember to visit our website to learn more about our upcoming events and
talks, or sign up to our newsletter ([register now](https://goo.gl/kP9bKW)).
